defmodule ProteinTranslation do
  @doc """
  Given an RNA string, return a list of proteins specified by codons, in order.
  """
  @spec of_rna(String.t()) :: { atom,  list(String.t()) }
  def of_rna(rna) do
    {status, acc} = check_rna(rna, [])
    cond do
      status == :error -> {:error, "invalid RNA"}
      true -> {:ok, acc}
    end
  end

  defp check_rna("", acc), do: {:ok, acc}
  defp check_rna(strand, acc) do
    {codon, tail} = String.split_at(strand, 3)
    {status, peptide} = of_codon(codon)
    cond do
      status == :error -> {:error, "invalid codon"}
      peptide == "STOP" -> {:ok, acc}
      true -> check_rna(tail, acc ++ [peptide])
    end
  end

  @doc """
  Given a codon, return the corresponding protein
"""
@proteins %{
  "UGU" => "Cysteine",
  "UGC" => "Cysteine",
  "UUA" => "Leucine",
  "UUG" => "Leucine",
  "AUG" => "Methionine",
  "UUU" => "Phenylalanine",
  "UUC" => "Phenylalanine",
  "UCU" => "Serine",
  "UCC" => "Serine",
  "UCA" => "Serine",
  "UCG" => "Serine",
  "UGG" => "Tryptophan",
  "UAU" => "Tyrosine",
  "UAC" => "Tyrosine",
  "UAA" => "STOP",
  "UAG" => "STOP",
  "UGA" => "STOP"
}

  @spec of_codon(String.t()) :: { atom, String.t() }
  def of_codon(codon) do
    cond do
      Map.has_key?(@proteins, codon) -> {:ok, @proteins[codon]}
      true -> {:error, "invalid codon"}
    end
  end
end

