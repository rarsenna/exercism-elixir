defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  @upper_case_start 65
  @upper_case_end 90
  @lower_case_start 97
  @lower_case_end 122
  @alphabet_length 26

  defp _is_upper_case?(x) do
    x in @upper_case_start..@upper_case_end
  end

  defp _is_lower_case?(x) do
    x in @lower_case_start..@lower_case_end
  end

  defp _translate(x, shift) do
    cond do
      _is_upper_case?(x) -> 
        rem(x-@upper_case_start+shift, @alphabet_length) + @upper_case_start
      _is_lower_case?(x) ->
        rem(x-@lower_case_start+shift, @alphabet_length) + @lower_case_start
      true ->
        x
    end
  end

  def rotate(text, shift) do
    text
    |> to_charlist
    |> Enum.map(fn x -> _translate(x, shift) end)
    |> List.to_string
  end

end

