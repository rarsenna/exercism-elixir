defmodule SumOfMultiples do
  @doc """
  Adds up all numbers from 1 to a given end number that are multiples of the factors provided.
  """
  @spec to(non_neg_integer, [non_neg_integer]) :: non_neg_integer
  def to(1, _), do: 0 
  def to(limit, factors) do
    2..limit - 1
     |> Enum.reduce(0, fn(x, acc) -> if is_factor?(x, factors), do: acc + x, else: acc end)
  end
  
  defp is_factor?(n, factors) do
    factors
      |> Enum.any?(&(rem(n, &1) == 0))
  end
end
