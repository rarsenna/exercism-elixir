defmodule Prime do

  @doc """
  Generates the nth prime.
  """
  @spec nth(non_neg_integer) :: non_neg_integer
  def nth(count) when count < 1, do: raise "Invalid input"
  def nth(count) do
    Stream.interval(1)
    |> Stream.drop(2)
    |> Stream.filter(&is_prime?/1)
    |> Enum.take(count)
    |> List.last
  end

  defp is_prime?(i) do
    2..i
      |> Enum.filter(fn(x) -> rem(i, x) == 0 end)
      |> length == 1
  end
end
