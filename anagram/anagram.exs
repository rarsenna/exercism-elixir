defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t, [String.t]) :: [String.t]
  def match(base, candidates) do
    candidates
      |> Enum.filter(fn(x) ->
        processor(base) == processor(x)
        &&
        String.downcase(base) != String.downcase(x)
      end)
  end

  @spec processor(String.t) :: list
  defp processor(word) do
    word
      |> String.downcase
      |> String.graphemes
      |> Enum.sort
  end
end
