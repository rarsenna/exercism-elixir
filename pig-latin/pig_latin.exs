defmodule PigLatin do
  @doc """
  Given a `phrase`, translate it a word at a time to Pig Latin.

  Words beginning with consonants should have the consonant moved to the end of
  the word, followed by "ay".

  Words beginning with vowels (aeiou) should have "ay" added to the end of the
  word.

  Some groups of letters are treated like consonants, including "ch", "qu",
  "squ", "th", "thr", and "sch".

  Some groups are treated like vowels, including "yt" and "xr".
  """
  @spec translate(phrase :: String.t()) :: String.t()
  def translate(phrase) do
    phrase
    |> String.split(" ")
    |> Enum.map(&(_translator(&1)))
    |> Enum.join(" ")
  end

  defp _vowel?(word) do
    word
    |> String.starts_with?(["a", "e", "i", "o", "u", "yt", "xr"])
  end

  defp _translator(word) do
    cond do
      _vowel?(word) ->
        word <> "ay"
      true ->
        [start, rest] = String.split(word, ~r{(ch)|(qu)|(squ)|(thr)|(th)|(sch)|([^aeiou])}, parts: 2, trim: true, include_captures: true)
        rest <> start <> "ay"
    end
  end
end

