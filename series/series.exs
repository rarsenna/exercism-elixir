defmodule StringSeries do
  @doc """
  Given a string `s` and a positive integer `size`, return all substrings
  of that size. If `size` is greater than the length of `s`, or less than 1,
  return an empty list.
  """
  @spec slices(s :: String.t(), size :: integer) :: list(String.t())
  def slices(s, size) do
    cond do
      size <= 0 || String.length(s) < size -> []
      true -> tokenizer([], s, 0, size)  
    end
  end

  defp tokenizer(list, string, start, length) do
    cond do
      String.length(string) < (start + length) -> list
      true -> tokenizer(
        list ++ [String.slice(string, start, length)], string, start + 1, length
        )
    end
  end
end