defmodule Roman do
  @doc """
  Convert the number to a roman number.
  """
  @spec numerals(pos_integer) :: String.t
  def numerals(number) do
    to_numerals(number, "")
  end

  defp to_numerals(0, string), do: string
  defp to_numerals(number, string) when number >= 1000, do: to_numerals(number - 1000, string <> "M")
  defp to_numerals(number, string) when number >= 900, do: to_numerals(number - 900, string <> "CM")
  defp to_numerals(number, string) when number >= 500, do: to_numerals(number - 500, string <> "D")
  defp to_numerals(number, string) when number >= 400, do: to_numerals(number - 400, string <> "CD")
  defp to_numerals(number, string) when number >= 100, do: to_numerals(number - 100, string <> "C")
  defp to_numerals(number, string) when number >= 90, do: to_numerals(number - 90, string <> "XC")
  defp to_numerals(number, string) when number >= 50, do: to_numerals(number - 50, string <> "L") 
  defp to_numerals(number, string) when number >= 40, do: to_numerals(number - 40, string <> "XL")
  defp to_numerals(number, string) when number >= 10, do: to_numerals(number - 10, string <> "X")
  defp to_numerals(number, string) when number >= 9, do: to_numerals(number - 9, string <> "IX")
  defp to_numerals(number, string) when number >= 5, do: to_numerals(number - 5, string <> "V")
  defp to_numerals(number, string) when number >= 4, do: to_numerals(number - 4, string <> "IV")
  defp to_numerals(number, string) when number >= 1, do: to_numerals(number - 1, string <> "I")
end
