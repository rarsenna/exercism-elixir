defmodule SpaceAge do
  @type planet :: :mercury | :venus | :earth | :mars | :jupiter
                | :saturn | :uranus | :neptune

  @doc """
  Return the number of years a person that has lived for 'seconds' seconds is
  aged on 'planet'.
  """

  @earth_year_to_seconds 31557600
  @other_planets_year_to_seconds %{
    mercury: 0.2408467 * @earth_year_to_seconds,
    venus: 0.61519726 * @earth_year_to_seconds,
    mars: 1.8808158 * @earth_year_to_seconds,
    jupiter: 11.862615 * @earth_year_to_seconds,
    saturn: 29.447498 * @earth_year_to_seconds,
    uranus: 84.016846 * @earth_year_to_seconds,
    neptune: 164.79132 * @earth_year_to_seconds
  }

  @spec age_on(planet, pos_integer) :: float
  def age_on(:earth, seconds), do: seconds / @earth_year_to_seconds
  def age_on(planet, seconds) do
    seconds / @other_planets_year_to_seconds[planet]
  end
end
