defmodule Hamming do
  @doc """
  Returns number of differences between two strands of DNA, known as the Hamming Distance.

  ## Examples

  iex> Hamming.hamming_distance('AAGTCATA', 'TAGCGATC')
  {:ok, 4}
  """
  @spec hamming_distance([char], [char]) :: non_neg_integer
  def hamming_distance(strand1, strand2) do
    cond do
      length(strand1) == length(strand2) ->
        calculate_distance(strand1, strand2, 0)
      true ->
        {:error, "Lists must be the same length"}
    end
  end

  defp calculate_distance([], [], distance), do: {:ok, distance}
  defp calculate_distance([h1|t1], [h2|t2], distance) do
    cond do
      h1 == h2 ->
        calculate_distance(t1, t2, distance)
      true ->
        calculate_distance(t1, t2, distance + 1)
    end
  end
end
